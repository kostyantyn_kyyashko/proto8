<?php
function js_common() {
    wp_enqueue_script('admin-bar.min.js', plugin_dir_url(__FILE__).'js/admin-bar.min.js');
    wp_enqueue_script('jqueryui', plugin_dir_url(__FILE__).'js/jqueryui.js');
    wp_enqueue_script('moment', plugin_dir_url(__FILE__).'js/moment-with-locales.min.js');
    wp_enqueue_script('datepicker', plugin_dir_url(__FILE__).'js/bootstrap-datepicker.js');
    wp_enqueue_script('confirm', plugin_dir_url(__FILE__).'js/jquery-confirm.js');
    wp_enqueue_script('crm', plugin_dir_url(__FILE__).'js/crm.js');
    wp_enqueue_script('backbone.marionette.min.js', plugin_dir_url(__FILE__).'index1_files/backbone.marionette.min.js');
    wp_enqueue_script('backbone.min.js', plugin_dir_url(__FILE__).'index1_files/backbone.min.js');
    wp_enqueue_script('backbone.radio.min.js', plugin_dir_url(__FILE__).'index1_files/backbone.radio.min.js');
    wp_enqueue_script('bootstrap.min.js', plugin_dir_url(__FILE__).'index1_files/bootstrap.min.js');
    wp_enqueue_script('cart-fragments.min.js', plugin_dir_url(__FILE__).'index1_files/cart-fragments.min.js');
    wp_enqueue_script('cherry-handler.min.js', plugin_dir_url(__FILE__).'index1_files/cherry-handler.min.js');
    wp_enqueue_script('cherry-js-core.min.js', plugin_dir_url(__FILE__).'index1_files/cherry-js-core.min.js');
    wp_enqueue_script('chosen.jquery.min.js', plugin_dir_url(__FILE__).'index1_files/chosen.jquery.min.js');
    wp_enqueue_script('common.min.js', plugin_dir_url(__FILE__).'index1_files/common.min.js');
    wp_enqueue_script('common-modules.min.js', plugin_dir_url(__FILE__).'index1_files/common-modules.min.js');
    wp_enqueue_script('core.min.js', plugin_dir_url(__FILE__).'index1_files/core.min.js');
    wp_enqueue_script('datepicker.min.js', plugin_dir_url(__FILE__).'index1_files/datepicker.min.js');
    wp_enqueue_script('dialog.min.js', plugin_dir_url(__FILE__).'index1_files/dialog.min.js');
    wp_enqueue_script('EasePack.min.js', plugin_dir_url(__FILE__).'index1_files/EasePack.min.js');
    wp_enqueue_script('frontend.min.js', plugin_dir_url(__FILE__).'index1_files/frontend.min.js');
    wp_enqueue_script('frontend-modules.min.js', plugin_dir_url(__FILE__).'index1_files/frontend-modules.min.js');
    wp_enqueue_script('hoverintent-js.min.js', plugin_dir_url(__FILE__).'index1_files/hoverintent-js.min.js');
    wp_enqueue_script('himagesloaded.min.js', plugin_dir_url(__FILE__).'index1_files/imagesloaded.min.js');
    wp_enqueue_script('jet-elements.js', plugin_dir_url(__FILE__).'index1_files/jet-elements.js');
    wp_enqueue_script('jet-tabs-frontend.min.js', plugin_dir_url(__FILE__).'index1_files/jet-tabs-frontend.min.js');
    wp_enqueue_script('jquery.ajaxsearchpro-noui-isotope.min.js', plugin_dir_url(__FILE__).'index1_files/jquery.ajaxsearchpro-noui-isotope.min.js');
    wp_enqueue_script('jquery.blockUI.min.js', plugin_dir_url(__FILE__).'index1_files/jquery.blockUI.min.js');
    wp_enqueue_script('jquery.js', plugin_dir_url(__FILE__).'index1_files/jquery.js');
    wp_enqueue_script('jquery.timepicker.min.js', plugin_dir_url(__FILE__).'index1_files/jquery.timepicker.min.js');
    wp_enqueue_script('jquery-confirm.min.js', plugin_dir_url(__FILE__).'index1_files/jquery-confirm.min.js');
    wp_enqueue_script('jquery-migrate.min.js', plugin_dir_url(__FILE__).'index1_files/jquery-migrate.min.js');
    wp_enqueue_script('jquery-ui.js', plugin_dir_url(__FILE__).'index1_files/jquery-ui.js');
    wp_enqueue_script('masonry.min.js', plugin_dir_url(__FILE__).'index1_files/masonry.min.js');
    wp_enqueue_script('moment-with-locales.min.js', plugin_dir_url(__FILE__).'index1_files/moment-with-locales.min.js');
    wp_enqueue_script('mouse.min.js', plugin_dir_url(__FILE__).'index1_files/mouse.min.js');
    wp_enqueue_script('photostack.js', plugin_dir_url(__FILE__).'index1_files/photostack.js');
    wp_enqueue_script('position.min.js', plugin_dir_url(__FILE__).'index1_files/position.min.js');
    wp_enqueue_script('scripts.js', plugin_dir_url(__FILE__).'index1_files/scripts.js');
    wp_enqueue_script('share-link.min.js', plugin_dir_url(__FILE__).'index1_files/share-link.min.js');
    wp_enqueue_script('share-link.min.js', plugin_dir_url(__FILE__).'index1_files/share-link.min.js');
    wp_enqueue_script('theme.bundle.min.js', plugin_dir_url(__FILE__).'index1_files/theme.bundle.min.js');
    wp_enqueue_script('TweenMax.min.js', plugin_dir_url(__FILE__).'index1_files/TweenMax.min.js');
    wp_enqueue_script('underscore.min.js', plugin_dir_url(__FILE__).'index1_files/underscore.min.js');
    wp_enqueue_script('waypoints.min.js', plugin_dir_url(__FILE__).'index1_files/waypoints.min.js');
    wp_enqueue_script('widget.min.js', plugin_dir_url(__FILE__).'index1_files/widget.min.js');
    wp_enqueue_script('woo.js', plugin_dir_url(__FILE__).'index1_files/woo.js');
    wp_enqueue_script('woocommerce.min.js', plugin_dir_url(__FILE__).'index1_files/woocommerce.min.js');
    wp_enqueue_script('wp-embed.min.js', plugin_dir_url(__FILE__).'index1_files/wp-embed.min.js');
}
function css_common() {
    wp_enqueue_style('font-awesome', '/wp-content/plugins/relotis-crm/js/bootstrap.bundle.js');
    wp_enqueue_style('bootstrapgrid', plugin_dir_url(__FILE__).'css/bootstrap-grid.css');
    wp_enqueue_style('fontawesome', plugin_dir_url(__FILE__).'css/font-awesome.css');
    wp_enqueue_style('bootstrapreboot', plugin_dir_url(__FILE__).'css/bootstrap-reboot.css');
    wp_enqueue_style('bootstrapdatepeacker', plugin_dir_url(__FILE__).'css/bootstrap-datepicker.css');
    wp_enqueue_style('jqueryuii', plugin_dir_url(__FILE__).'css/jquery-ui.css');
    wp_enqueue_style('jqueryui', plugin_dir_url(__FILE__).'css/jquery-confirm.min.css');
    wp_enqueue_style('admin-bar.min.css', plugin_dir_url(__FILE__).'css/admin-bar.min.css');
    wp_enqueue_style('animations.min.css', plugin_dir_url(__FILE__).'css/animations.min.css');
    wp_enqueue_style('bootstrap-datetimepicker.min.css', plugin_dir_url(__FILE__).'css/bootstrap-datetimepicker.min.css');
    wp_enqueue_style('cherry-handler-styles.min.css', plugin_dir_url(__FILE__).'css/cherry-handler-styles.min.css');
    wp_enqueue_style('chosen.css', plugin_dir_url(__FILE__).'css/chosen.css');
    wp_enqueue_style('common.min.css', plugin_dir_url(__FILE__).'css/common.min.css');
    wp_enqueue_style('dashicons.min.css', plugin_dir_url(__FILE__).'css/dashicons.min.css');
    wp_enqueue_style('elementor-icons.min.css', plugin_dir_url(__FILE__).'css/elementor-icons.min.css');
    wp_enqueue_style('frontend.min.css', plugin_dir_url(__FILE__).'css/frontend.min.css');
    wp_enqueue_style('global.css', plugin_dir_url(__FILE__).'css/global.css');
    wp_enqueue_style('jet-elements.css', plugin_dir_url(__FILE__).'css/jet-elements.css');
    wp_enqueue_style('jet-elements-skin.css', plugin_dir_url(__FILE__).'css/jet-elements-skin.css');
    wp_enqueue_style('jet-tabs-frontend.css', plugin_dir_url(__FILE__).'css/jet-tabs-frontend.css');
    wp_enqueue_style('jquery.timepicker.min.css', plugin_dir_url(__FILE__).'css/jquery.timepicker.min.css');
    wp_enqueue_style('jquery-confirm.min.css', plugin_dir_url(__FILE__).'css/jquery-confirm.min.css');
    wp_enqueue_style('jquery-ui.css', plugin_dir_url(__FILE__).'css/jquery-ui.css');
    wp_enqueue_style('juxtapose.css', plugin_dir_url(__FILE__).'css/juxtapose.css');
    wp_enqueue_style('post-647.css', plugin_dir_url(__FILE__).'css/post-647.css');
    wp_enqueue_style('slider-pro.min.css', plugin_dir_url(__FILE__).'css/pslider-pro.min.css');
    wp_enqueue_style('style.basic.css', plugin_dir_url(__FILE__).'css/style.basic.css');
    wp_enqueue_style('style.instances.css', plugin_dir_url(__FILE__).'css/style.instances.css');
    wp_enqueue_style('style.min(1).css', plugin_dir_url(__FILE__).'css/style.min(1).css');
    wp_enqueue_style('styles.css', plugin_dir_url(__FILE__).'css/styles.css');
    wp_enqueue_style('styles.css', plugin_dir_url(__FILE__).'css/styles.css');
    wp_enqueue_style('theme.bundle.min.css', plugin_dir_url(__FILE__).'css/theme.bundle.min.css');
    wp_enqueue_style('woocommerce.css', plugin_dir_url(__FILE__).'css/woocommerce.css');
    wp_enqueue_style('woocommerce-layout.css', plugin_dir_url(__FILE__).'css/woocommerce-layout.css');
    wp_enqueue_style('woocommerce-smallscreen.css', plugin_dir_url(__FILE__).'css/woocommerce-smallscreen.css');
        wp_enqueue_style('bootstrap.min.css', plugin_dir_url(__FILE__).'css/bootstrap.min.css');

}
/*    js_common();
    css_common();
    add_action('wp_head', 'css_common');
    add_action('wp_footer', 'js_common');
    add_action('admin_menu', 'css_common');
    add_action('admin_menu', 'js_common');*/
    wp_enqueue_script('jq', plugin_dir_url(__FILE__).'js/jquery.js');
    wp_enqueue_style('font-awesome.min.css', plugin_dir_url(__FILE__).'css/font-awesome.css?ver=5.4.1');
    wp_enqueue_script('cookie', plugin_dir_url(__FILE__).'index1_files/jquery.cookie.js');
    wp_enqueue_script('bootstrap', plugin_dir_url(__FILE__).'js/bootstrap.js');
    wp_enqueue_style('bootstrap450', plugin_dir_url(__FILE__).'css/bootstrap.css');

    wp_enqueue_script('crm', plugin_dir_url(__FILE__).'js/crm.js');

    function send_to_crm() {
    ?>
    <script>
    $.removeCookie('div_stage_button_id');
    $.removeCookie('stage');
    $.removeCookie('transaction_id');
    $.removeCookie('wp_user_id');
    $.removeCookie('div_stage_button_id');
    $.removeCookie('wp_user_name');
</script>
<?
    $current_user = wp_get_current_user();
    if( $current_user->ID ){
        ?>
        <script>
        var wp_user_id = <?=$current_user->ID?>,
        wp_user_name = '<?=$current_user->user_login?>',
        ajaxurl="<?=admin_url('admin-ajax.php')?>";

        $(document).ready(function() {
            var html = '<button id="crm_button" class="btn btn-warning form-control">Transfer to CRM</button>';
            $('.product_meta').append(html);
            setTimeout(function() {
              transfer_to_crm();
            }, 200)
        })
        </script>
        <?
    }
}

function init_transaction()
{
    global $wpdb;
    $price_value =  intval(preg_replace('/[^\d]/', '', $_POST['price']))/100;
    $price_currency =  preg_replace('/[\d\.\,]/', '', $_POST['price']);
    $transaction_id = mt_rand(1000, 1000000);
    $wpdb->insert('crm_analysis', [
        'wp_user_id' => $_POST['wp_user_id'],
        'wp_user_name' => $_POST['wp_user_name'],
        'transaction_id' => $transaction_id,
        'price_value' => $price_value,
        'price_currency' => $price_currency,
        'address' => $_POST['address'],
        'description' => $_POST['description'],
        'image_src' => $_POST['image_src'],
        'object_url' => $_POST['object_url'],
        'begin_at' => time(),
        'status' => "Begin"
    ]);
    $wpdb->insert('crm_stage_complete', [
            'transaction_id' => $transaction_id,
            'stage' => 'Begin',
            'status' => 'complete',
]);
    $resp = json_encode([
        'status' => 'ok',
        'transaction_id' => $transaction_id,
        'wp_user_id' => $_POST['wp_user_id'],
        'wp_user_name' => $_POST['wp_user_name'],
    ]);
    die($resp);
}

function crm_db_init(){
    global $wpdb;
    $query = <<<sql
CREATE TABLE crm_analysis (
  id int(11) NOT NULL AUTO_INCREMENT,
  wp_user_id int(11) NOT NULL,
  wp_user_name varchar(255) DEFAULT NULL,
  transaction_id int(11) DEFAULT NULL,
  price_value int(11) NOT NULL,
  price_currency varchar(5) NOT NULL,
  address varchar(255) NOT NULL,
  description text DEFAULT NULL,
  image_src varchar(255) DEFAULT NULL,
  object_url varchar(255) DEFAULT NULL,
  begin_at int(11) DEFAULT NULL,
  crm_comment text DEFAULT NULL,
  crm_owner text DEFAULT NULL,
  transaction_name varchar(255) DEFAULT NULL,
  close_at int(11) DEFAULT NULL,
  status varchar(255) DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE INDEX UK_crm_objects_transaction_id (transaction_id)
)
ENGINE = INNODB
AUTO_INCREMENT = 12
AVG_ROW_LENGTH = 1820
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

CREATE TABLE crm_close (
  id int(11) NOT NULL AUTO_INCREMENT,
  transaction_id int(11) DEFAULT NULL,
  transaction_close_at varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

CREATE TABLE crm_contacts (
  id int(11) NOT NULL AUTO_INCREMENT,
  transaction_id int(11) DEFAULT NULL,
  name varchar(50) DEFAULT NULL,
  company varchar(255) DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  phone varchar(255) DEFAULT NULL,
  email varchar(50) DEFAULT NULL,
  PRIMARY KEY (id),
  INDEX IDX_crm_contacts_transaction_i (transaction_id),
  UNIQUE INDEX UK_crm_contacts (id, transaction_id)
)
ENGINE = INNODB
AUTO_INCREMENT = 5
AVG_ROW_LENGTH = 8192
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

CREATE TABLE crm_negotiations (
  id int(11) NOT NULL AUTO_INCREMENT,
  transaction_id int(11) DEFAULT NULL,
  date int(11) DEFAULT NULL,
  time varchar(255) DEFAULT NULL,
  person_present text DEFAULT NULL,
  comment text DEFAULT NULL,
  result text DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 5
AVG_ROW_LENGTH = 4096
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

CREATE TABLE crm_proposal (
  id int(11) NOT NULL AUTO_INCREMENT,
  transaction_id int(11) DEFAULT NULL,
  proposal_date varchar(255) DEFAULT NULL,
  proposal_time varchar(255) DEFAULT NULL,
  proposal_whom varchar(255) DEFAULT NULL,
  proposal_text text DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE INDEX UK_crm_proposal_transaction_id (transaction_id)
)
ENGINE = INNODB
AUTO_INCREMENT = 7
AVG_ROW_LENGTH = 8192
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

CREATE TABLE crm_stage_complete (
  id int(11) NOT NULL AUTO_INCREMENT,
  transaction_id int(11) DEFAULT NULL,
  stage varchar(255) DEFAULT NULL,
  status varchar(255) DEFAULT 'no_complete',
  PRIMARY KEY (id),
  UNIQUE INDEX UK_crm_stage_complete (transaction_id, stage)
)
ENGINE = INNODB
AUTO_INCREMENT = 21
AVG_ROW_LENGTH = 910
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

CREATE TABLE crm_tasks (
  id int(11) NOT NULL AUTO_INCREMENT,
  stage varchar(255) DEFAULT NULL,
  transaction_id int(11) DEFAULT NULL,
  task varchar(255) DEFAULT NULL,
  executor varchar(255) DEFAULT NULL,
  date varchar(255) DEFAULT NULL,
  time varchar(255) DEFAULT NULL,
  result text DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 14
AVG_ROW_LENGTH = 1365
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;
sql;
    $wpdb->query($query);
}

function crm_file_init()
{
    $file1path = plugin_dir_path(__FILE__) . 'header.html';
    $file1_content = file_get_contents($file1path);
    $url = get_site_url();
    $url = str_replace('http://', '', $url);
    $url = str_replace('https://', '', $url);
    $file1_content = str_replace('proto8.relotis.com', $url, $file1_content);
    file_put_contents($file1path, $file1_content);

    $file2path = plugin_dir_path(__FILE__) . 'header.html';
    $file2_content = file_get_contents($file2path);
    $url = get_site_url();
    $url = str_replace('http://', '', $url);
    $url = str_replace('https://', '', $url);
    $file2_content = str_replace('proto8.relotis.com', $url, $file2_content);
    file_put_contents($file2path, $file2_content);
}

function relotis_current_user()
{
    $user = wp_get_current_user();
    echo $user->user_login;
}

function analysis()
{
    global $wpdb;
    $current_user = wp_get_current_user();
    $transaction_id = $_COOKIE['transaction_id']?$_COOKIE['transaction_id']:null;
    $stage = $_COOKIE['stage']?$_COOKIE['stage']:null;
    if((!$transaction_id || !$current_user->ID)) {
        ?>
        <script>
            alert('Authentification Need')
            window.location.href='/';
        </script>
        <?
    }
    $row = $wpdb->get_results(("SELECT * FROM crm_analysis WHERE transaction_id = $transaction_id"))[0];
    $task_rows = $wpdb->get_results(("SELECT * FROM crm_tasks WHERE 
transaction_id = $transaction_id AND stage='{$stage}'"));

    ?>
    <style>
        .right150 {width: 150px; text-align: right;}
        .left400 {width: 400px; text-align: left;}
        .stage_wrapper {margin: 0 auto; width: 1000px;}
        th, td {padding-left: 10px;}
    </style>
    <div data-stage="Analysis" class="stage_wrapper">
        <table class="table" style="width: 100%;">
            <tbody>
            <tr>
                <td><h4 style="text-align: right;">Transaction Name:</h4></td>
                <td><input id="transaction_name" type="text" class="form-control" style="width: 300px;" value="<?=$row->transaction_name?>"></td>
            </tr>
            <tr>
                <td><h4 style="text-align: right;">Owner:</h4></td>
                <td><input id="crm_owner" type="text" class="form-control" style="width: 300px;" value="<?=$row->crm_owner?>"></td>
            </tr>
            </tbody>
        </table>
        <table class="table table-condensed table-striped table-bordered table-hover" style="width: 100%; margin: 0 auto 20px auto;">
            <table class="table table-condensed table-striped table-bordered table-hover" style="width: 100%; margin: 0 auto 20px auto;">
                <thead>
                <tr>
                    <th>Object</th>
                    <th>ID</th>
                    <th>Price, $</th>
                    <th>Address</th>
                    <th>Date</th>
                    <th>Comment</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        <a href="<?=$row->object_url?>" target="_blank">
                            <img src="<?=$row->image_src?>" style="width: 150px;">
                        </a>
                    </td>
                    <td style="vertical-align: middle;"><?=$row->transaction_id?></td>
                    <td style="vertical-align: middle;"><?=$row->price_value?></td>
                    <td style="vertical-align: middle;"><?=$row->address?></td>
                    <td style="vertical-align: middle;" id="#td_date"><input class="form-control datepicker" name="date2"
                                 style="width: 110px;"  value="<?=date('m/d/Y', $row->begin_at)?>"></td>
                    <td style="vertical-align: middle;"><textarea id="crm_comment"><?=$row->crm_comment?></textarea></td>
                </tr>
                <tr>
                    <td colspan="5" style="text-align: right"><button class="btn btn-info" id="analysis_refresh_data"  data-stage="Analysis" data-transaction-id>Save Data</button></td>
                    <td><button class="btn btn-warning stage_complete" data-stage="Analysis">Mark this stage as complete</button></td>
                </tr>
                </tbody>
            </table>

    <?tasks_table('analysis');?>

    </div>

    <?
}

function contacts()
{
    global $wpdb;
    $user_id = wp_get_current_user()->ID;
    $transaction_id = $_COOKIE['transaction_id'];
    ?>
    <div style="margin: 0 auto; width: 1030px;" class="stage_wrapper" data-stage="Contacts">
    <table class="table table-bordered table-condensed table-striped" style="width: auto;">
        <thead>
        <tr>
            <th style="width: 150px;">Name</th>
            <th style="width: 150px;">Company</th>
            <th style="width: 150px;">Position</th>
            <th style="width: 150px;">Phone</th>
            <th style="width: 150px;">Email</th>
            <th style="width: 50px;"></th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td><input type="text" class="form-control" placeholder="Name" name="name"></td>
            <td><input type="text" class="form-control" placeholder="Company" name="company"></td>
            <td>
                <select type="text" class="form-control" placeholder="Position" name="position">
                    <option value="manager">Manager</option>
                    <option value="director">Director</option>
                    <option value="owner">Owner</option>
                    <option value="other">Other</option>
                </select>
            </td>
            <td><input type="text" class="form-control" placeholder="Phone" name="phone"></td>
            <td><input type="email" class="form-control" placeholder="Email" name="email"></td>
            <td style="cursor: pointer;" id="add_contact"><i style="color: blue;" class="fa fa-plus-square fa-2x"></i></td>
        </tr>
        <?
        $contacts = $wpdb->get_results("SELECT * FROM crm_contacts WHERE transaction_id = {$transaction_id}");
        foreach ($contacts as $contact):
        ?>
        <tr>
            <td><?=$contact->name?></td>
            <td><?=$contact->company?></td>
            <td><?=$contact->position?></td>
            <td><?=$contact->phone?></td>
            <td><?=$contact->email?></td>
            <td style="cursor: pointer;"  id="<?=$contact->id?>" class="remove_contact"><i style="color: orange;" class="fa fa-trash fa-2x"></i></td>
        </tr>
        <? endforeach;?>
        </tbody>
    </table>
    </div>
    <?
}

function add_contact_to_db()
{
    global $wpdb;
    $wpdb->insert('crm_contacts', [
            'transaction_id' => $_POST['transaction_id'],
            'name' => $_POST['name'],
            'company' => $_POST['company'],
            'position' => $_POST['position'],
            'phone' => $_POST['phone'],
            'email' => $_POST['email'],
    ]);
    die('ok');
}

function remove_contact_from_db()
{
    global $wpdb;
    $sql = <<<sql
DELETE FROM crm_contacts WHERE id = {$_POST['id']}
sql;
    $wpdb->query($sql);
    die('ok');
}

function proposal()
{
    global $wpdb;
    $user_id = wp_get_current_user()->ID;
    $transaction_id = $_COOKIE['transaction_id'];
    $row = $wpdb->get_row("
    SELECT * from crm_proposal WHERE transaction_id = '{$transaction_id}'
    ");
    ?>
    <div style="margin: 0 auto; width: 1030px;" class="stage_wrapper" data-stage="Proposal">

    <table class="table table-bordered table-condensed table-striped table-hover" style="width: 100%;">
        <thead>
        <tr>
            <th>
                Date
            </th>
            <th>
                Time
            </th>
            <th>
                To whom the proposal was made
            </th>
            <th>
                Proposal
            </th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <?if (isset($row->proposal_date)) {
                $proposal_date = date('m/d/Y', $row->proposal_date);
            }
            else {$proposal_date = '';
            }  ?>

            <td><input class="form-control datepicker"  style="width: 110px;" data="proposal_date" value="<?=$proposal_date??''?>"></td>
            <td><input class="form-control timepicker"  style="width: 110px;" data="proposal_time" value="<?=$row->proposal_time??''?>"></td>
            <td><textarea style="width: 100%;" placeholder="whom" id="proposal_whom"><?=$row->proposal_whom??''?></textarea> </td>
            <td><textarea style="width: 100%;" placeholder="proposal"  id="proposal_text"><?=$row->proposal_text??''?></textarea></td>
        </tr>
        <tr>
            <td class="right150" colspan="3">
            <button class="btn btn-info" id="proposal_refresh_data"  data-stage="Proposal" data-transaction-id>Save Data</button>
            </td>
            <td class="left400">
            <button class="btn btn-warning stage_complete"  data-transaction-id="<?=$transaction_id?>" data-stage="Proposal">
            Mark this stage as Complete
            </button>
            </td>
        </tr>
        </tbody>
    </table>
    <?tasks_table('proposal')?>
    </div>
    <?

}

function negotiation()
{
    global $wpdb;
    $user_id = wp_get_current_user()->ID;
    $transaction_id = $_COOKIE['transaction_id'];
    $negotiations = $wpdb->get_results("SELECT * FROM crm_negotiations WHERE transaction_id = {$transaction_id}");
    ?>

    <div style="margin: 0 auto; width: 1030px;" class="stage_wrapper"  data-stage="Negotiations">
    <hr>
        <h4 class="stage_tasks_flow">Negotiations Flow</h4>
        <table class="table table-bordered table-condensed table-striped table-hover" style="width: 100%;">
        <thead>
        <tr>
            <th style="padding-left: 10px;">
                Date
            </th>
            <th>
                Time
            </th>
            <th>
                Persons present
            </th>
            <th>
                Comment
            </th>
            <th>
                Result
            </th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td style="padding-left: 10px;"><input class="form-control datepicker" data-nego="date" style="width: 110px;"></td>
            <td><input class="form-control timepicker" data-nego="time" style="width: 110px;"></td>
            <td><input style="width: 210px;" placeholder="Persons" data-nego="person_present"> </td>
            <td><textarea style="width: 210px;" placeholder="Comment" data-nego="comment"></textarea></td>
            <td><textarea style="width: 210px;" placeholder="Result" data-nego="result"></textarea></td>
        </tr>
        <? foreach ($negotiations as $n): ?>
        <tr>
            <td><input class="form-control datepicker" style="width: 110px;" value="<?=date('m/d/Y', $n->date)?>" disabled="disabled"></td>
            <td><input class="form-control timepicker" style="width: 110px;" value="<?=$n->time?>" disabled="disabled"></td>
            <td><textarea style="width: 210px;" placeholder="Persons" disabled="disabled"><?=$n->person_present?></textarea> </td>
            <td><textarea style="width: 210px;" placeholder="Comment" disabled="disabled"><?=$n->comment?></textarea></td>
            <td><textarea style="width: 210px;" placeholder="Result" disabled="disabled"><?=$n->result?></textarea></td>
        </tr>
        <? endforeach;?>
        <tr>
            <td class="right150" colspan="4"><button class="btn btn-info" id="negotiation_refresh_data">Save Data</button></td>
            <td class="left400">
            <button class="btn btn-warning stage_complete" data-transaction-id="<?=$transaction_id?>" data-stage="Negotiation">Mark this stage as complete</button>
            </td>
        </tr>
        </tbody>
    </table>
    <?tasks_table('negotiations');?>
    </div>

    <?
}

function close()
{
        global $wpdb;
    $current_user = wp_get_current_user();
    $transaction_id = $_COOKIE['transaction_id'];
    if((!$transaction_id || !$current_user->ID)) {
        ?>
        <script>
            alert('You must be authorized And coose object from listing');
            window.location.href='/'
        </script>
        <?
    }
    $row = $wpdb->get_results("SELECT * FROM crm_analysis WHERE transaction_id = $transaction_id")[0];
    ?>
        <div style="margin: 0 auto; width: 1030px;" class="stage_wrapper"  data-stage="Close">
        <div style="width: 1000px; margin: 0 auto">

            <table class="table table-condensed table-striped table-bordered table-hover" style="width: auto; margin: 0 auto 20px auto;">
                <thead>
                <tr>
                    <td>Object</td>
                    <th>ID</th>
                    <th style="width: 30px;">Price</th>
                    <th>Address</th>
                    <th>Begin at</th>
                    <th>Close at</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                    <td>
                        <a href="<?=$row->object_url?>" target="_blank">
                        <img src="<?=$row->image_src?>" style="width: 150px;">
                        </a>
                    </td>
                    <td style="vertical-align: middle;"><?=$row->transaction_id?></td>
                    <td style="vertical-align: middle;"><input type="text" id="price_value" class="form-control" value="<?=$row->price_value?>"></td>
                    <td style="vertical-align: middle;"><?=$row->address?></td>
                    <td style="vertical-align: middle;"><?=date('m/d/Y', $row->begin_at)?></td>
                    <td style="vertical-align: middle;"><input class="form-control datepicker" id="close_at" value="<?=date('m/d/Y', intval($row->close_at))?>"></td>
                    <td style="vertical-align: middle;"><button class="btn btn-danger" id="transaction_close_button">Mark transaction as complete</button> </td>
                </tbody>
            </table>
            <div>
    <?tasks_table('close');?>

    </div>

    <?
}

function analysis_refresh_data()
{
    global $wpdb;
    $transaction_id = $_POST['transaction_id'];
    $crm_owner = $_POST['crm_owner'];
    $crm_comment = $_POST['crm_comment'];
    $transaction_name = $_POST['transaction_name'];
    $begin_at =strtotime($_POST['begin_at']); //echo $begin_at;
    $sql = "UPDATE crm_analysis SET crm_owner = '{$crm_owner}', crm_comment = '{$crm_comment}', 
transaction_name = '{$transaction_name}', begin_at = '{$begin_at}'
WHERE transaction_id = {$transaction_id}";
    $wpdb->query(($sql));
    die('ok');
}

function proposal_refresh_data()
{
    global $wpdb;
    $transaction_id = $_GET['transaction_id'];
    $proposal_date = strtotime($_GET['proposal_date']);
    $proposal_time = $_GET['proposal_time'];
    $proposal_whom = $_GET['proposal_whom'];
    $proposal_text = $_GET['proposal_text'];
    $sql = <<<sql
REPLACE INTO crm_proposal
SET
transaction_id = {$transaction_id},
proposal_date = '{$proposal_date}',
proposal_time = '{$proposal_time}',
proposal_whom = '{$proposal_whom}',
proposal_text = '{$proposal_text}'
sql;
    $wpdb->query($sql);
    die('ok');
}

function negotiation_refresh_data()
{
    global $wpdb;
    $transaction_id = $_POST['transaction_id'];
    $date = strtotime($_POST['date']);
    $time = $_POST['time'];
    $person_present = $_POST['person_present'];
    $comment = $_POST['comment'];
    $result = $_POST['result'];
    $sql = <<<sql
REPLACE INTO crm_negotiations
SET
transaction_id = {$transaction_id},
date = '{$date}',
time = '{$time}',
person_present = '{$person_present}',
comment = '{$comment}',
result = '{$result}'
sql;
    $wpdb->query($sql);
    die('ok');
}

function set_stage_as_complete()
{
    global $wpdb;
    $stage = $_POST['stage'];
    $transaction_id = $_COOKIE['transaction_id'];
    $previous_table = [
        'Analysis' => 'Begin',
        'Proposal' => 'Analysis',
        'Negotiations' => 'Proposal',
        'Close' => 'Negotiations',
    ];
    $previous_stage = $previous_table[$stage];
        $sql2 = <<<sql
UPDATE crm_analysis SET status = '{$stage}' WHERE transaction_id = {$transaction_id}
sql;
        $wpdb->query($sql2);

    $sql1 = <<<sql
SELECT status FROM crm_stage_complete WHERE transaction_id = {$transaction_id} AND stage = '{$previous_stage}'
sql;
    $previous_stage_status = $wpdb->get_var($sql1);
    if ($previous_stage_status == 'complete' || $stage == 'Analysis') {
        $sql11 = <<<sql
REPLACE INTO crm_stage_complete 
(transaction_id, stage, status)
VALUES
({$transaction_id}, '{$stage}', 'complete') 
sql;
        $wpdb->query($sql11);die('ok');
    }
    $msg = 'You must to complete previous stage: ' . strtoupper($previous_stage);
    die($msg);
}

function check_stage_as_complete()
{
    global $wpdb;
    $stage = $_POST['stage'];
    $transaction_id = $_POST['transaction_id'];
    $sql = <<<sql
SELECT cpl.status FROM crm_stage_complete cpl 
WHERE
cpl.transaction_id = {$transaction_id} AND cpl.stage = '{$stage}' 
sql;
    $var = $wpdb->get_var($sql);
    die($var);
}

function set_transaction_complete()
{
    global $wpdb;
    $close_at = $_POST['close_at'];
    $transaction_id = $_POST['transaction_id'];
    $price_value = $_POST['price_value'];
    $close_at = strtotime($close_at);
    $sql = <<<sql
UPDATE crm_analysis
SET 
close_at = '{$close_at}',
price_value = {$price_value},
status = 'Close'
WHERE transaction_id = {$transaction_id}
sql;
    $wpdb->query($sql);
    die('ok');
}

function tasks_table($stage)
{
    global $wpdb;
    $transaction_id = $_COOKIE['transaction_id']??null;
    $task_rows = $wpdb->get_results(("SELECT * FROM crm_tasks WHERE 
transaction_id = {$transaction_id} AND stage = '{$stage}'"));

    ?>
     <hr style="height: 2px; width: 100%">
    <div class="tasks">
    <h4 class="stage_tasks_name"><?=mb_ucfirst($stage)?> Tasks</h4>
            <table class="table table-bordered table-striped table-hover" style="width: 100%;">
            <thead>
                    <tr>
                        <th style="padding-left: 10px;">Task</th>
                        <th>Executor</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Result</th>
                    </tr>
            </thead>
            <tbody>
                    <?foreach ($task_rows as $row):?>
                    <tr>
                        <td style="padding-left: 10px;"><textarea class="task form-control"><?=$row->task?></textarea> </td>
                        <td><input type="text" value="<?=$row->executor?>" class="executor form-control"></td>
                        <td style="width: 120px;"><input class="date datepicker" style="width: 110px;" value="<?=$row->date?>"></td>
                        <td style="width: 120px;"><input class="time timepicker" style="width: 110px;" value="<?=$row->time?>"></td>
                        <td><textarea class="result"><?=$row->result?></textarea> </td>
                    </tr>
                    <?endforeach;?>
                    <tr class="task_data">
                        <td style="padding-left: 10px;"><textarea class="task form-control" placeholder="Task Description"></textarea> </td>
                        <td><input type="text" value="" class="executor form-control"  placeholder="Who will work on this task"></td>
                        <td style="width: 120px;"><input class="date datepicker" style="width: 110px;"  placeholder="Begin date"></td>
                        <td style="width: 120px;"><input class="time timepicker" style="width: 110px;" value=""placeholder="Begin time"></td>
                        <td><textarea class="result" placeholder="Result of task"></textarea> </td>
                    </tr>
                    <tr>
                        <td colspan="4"></td>
                        <td><button class="btn btn-info save_task">Save Task</button> </td>
                    </tr>
            </tbody>
        </table>
</div>
    <?
}

function save_task()
{
    global $wpdb;
    $sql = <<<sql
INSERT INTO crm_tasks
(stage, transaction_id, task, executor, date, time, result)
VALUES 
('{$_POST['stage']}', {$_POST['transaction_id']}, '{$_POST['task']}', '{$_POST['executor']}', '{$_POST['date']}', '{$_POST['time']}', '{$_POST['result']}')
sql;
    $wpdb->query($sql);
    die('ok');
}

function mb_ucfirst($string, $encoding = 'UTF-8') {
    $firstChar = mb_strtoupper(mb_substr($string, 0, 1, $encoding), $encoding);
    return $firstChar . mb_substr($string, 1, mb_strlen($string, $encoding), $encoding);
}

function crm_header()
{
    ?>
    <div data-elementor-type="page" data-elementor-id="1277" class="elementor elementor-1277" data-elementor-settings="[]">
        <div class="elementor-inner">
            <div class="elementor-section-wrap">
                <section class="elementor-element elementor-element-922adef elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section jet-parallax-section" data-id="922adef" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-element elementor-element-b967eca elementor-column elementor-col-50 elementor-top-column" data-id="b967eca" data-element_type="column">
                                <div class="elementor-column-wrap  elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <section class="elementor-element elementor-element-82e61af elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section jet-parallax-section" data-id="82e61af" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-default">
                                                <div class="elementor-row">
                                                    <div class="elementor-element elementor-element-ef01ce3 elementor-column elementor-col-50 elementor-inner-column" data-id="ef01ce3" data-element_type="column">
                                                        <div class="elementor-column-wrap  elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-b884b1b elementor-widget elementor-widget-image" data-id="b884b1b" data-element_type="widget" data-widget_type="image.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-image">
                                                                            <img width="296" height="317" src="http://proto8.relotis.com/wp-content/uploads/2020/06/RelotisCRMMainLogoSmall.png" class="attachment-medium_large size-medium_large" alt="" srcset="http://proto8.relotis.com/wp-content/uploads/2020/06/RelotisCRMMainLogoSmall.png 296w, http://proto8.relotis.com/wp-content/uploads/2020/06/RelotisCRMMainLogoSmall-280x300.png 280w" sizes="(max-width: 296px) 100vw, 296px">											</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-element elementor-element-efee9ae elementor-column elementor-col-50 elementor-inner-column" data-id="efee9ae" data-element_type="column">
                                                        <div class="elementor-column-wrap  elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-8a6032e elementor-widget elementor-widget-heading" data-id="8a6032e" data-element_type="widget" data-widget_type="heading.default">
                                                                    <div class="elementor-widget-container">
                                                                        <h2 class="elementor-heading-title elementor-size-default">RELOTIS</h2>		</div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-e59ff2d elementor-widget elementor-widget-heading" data-id="e59ff2d" data-element_type="widget" data-widget_type="heading.default">
                                                                    <div class="elementor-widget-container">
                                                                        <h2 class="elementor-heading-title elementor-size-default">Real Estate</h2>		</div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-fac894e elementor-widget elementor-widget-heading" data-id="fac894e" data-element_type="widget" data-widget_type="heading.default">
                                                                    <div class="elementor-widget-container">
                                                                        <h2 class="elementor-heading-title elementor-size-default">Customer Relationship Management</h2>		</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        <section class="elementor-element elementor-element-443e66d elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section jet-parallax-section" data-id="443e66d" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-default">
                                                <div class="elementor-row">
                                                    <div class="elementor-element elementor-element-a593258 elementor-column elementor-col-50 elementor-inner-column" data-id="a593258" data-element_type="column">
                                                        <div class="elementor-column-wrap  elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-8295da5 elementor-widget elementor-widget-text-editor" data-id="8295da5" data-element_type="widget" data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-text-editor elementor-clearfix">Account Name</div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-e51cc7f elementor-widget elementor-widget-text-editor" data-id="e51cc7f" data-element_type="widget" data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-text-editor elementor-clearfix">[user_name2]</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-element elementor-element-33d0d10 elementor-column elementor-col-50 elementor-inner-column" data-id="33d0d10" data-element_type="column">
                                                        <div class="elementor-column-wrap  elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-38b9fe1 elementor-widget elementor-widget-text-editor" data-id="38b9fe1" data-element_type="widget" data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-text-editor elementor-clearfix"><p>Close Data</p></div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-70370c4 elementor-widget elementor-widget-text-editor" data-id="70370c4" data-element_type="widget" data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-text-editor elementor-clearfix"><?=do_shortcode('[close_data]')??'' ?></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-element elementor-element-3a14937 elementor-column elementor-col-50 elementor-top-column" data-id="3a14937" data-element_type="column">
                                <div class="elementor-column-wrap  elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <section class="elementor-element elementor-element-eff3130 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section jet-parallax-section" data-id="eff3130" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-default">
                                                <div class="elementor-row">
                                                    <div class="elementor-element elementor-element-3af2619 elementor-column elementor-col-50 elementor-inner-column" data-id="3af2619" data-element_type="column">
                                                        <div class="elementor-column-wrap  elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-b61df0c elementor-widget elementor-widget-text-editor" data-id="b61df0c" data-element_type="widget" data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-text-editor elementor-clearfix"><p>Ammount</p></div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-0d49b90 elementor-widget elementor-widget-text-editor" data-id="0d49b90" data-element_type="widget" data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-text-editor elementor-clearfix">[ammount]</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-element elementor-element-f7d0fe9 elementor-column elementor-col-50 elementor-inner-column" data-id="f7d0fe9" data-element_type="column">
                                                        <div class="elementor-column-wrap  elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-78481b0 elementor-widget elementor-widget-text-editor" data-id="78481b0" data-element_type="widget" data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-text-editor elementor-clearfix"><p>Opportunity Owner</p></div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-4875798 elementor-widget elementor-widget-text-editor" data-id="4875798" data-element_type="widget" data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-text-editor elementor-clearfix">[opportunity_owner]</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <?
}

function get_analysis_row()
{
    global $wpdb;
    $transaction_id = $_COOKIE['transaction_id'];
    $row = $wpdb->get_results(("SELECT * FROM crm_analysis WHERE transaction_id = $transaction_id"))[0];
    return $row;
}


function user_name()
{
    $row = get_analysis_row();
    echo $row->wp_user_name;

}

function close_data()
{
    $row = get_analysis_row();
    if (!$row->close_at || time() - $row->close_at > 0)
        echo '-';
    else {
        echo date('m/d/Y', $row->close_at);
    }

}

function ammount()
{
    $row = get_analysis_row();
    echo $row->price_value;
}

function opportunity_owner()
{
    $row = get_analysis_row();
    echo $row->crm_owner;
}


function select_count_stage()
{
    global $wpdb;
    $tables = [
        'crm_analysis',
        'crm_proposal',
        'crm_negotiations',
        'crm_close'
    ];
    $out = [];
    foreach ($tables as $table)
    {
        $sql = "SELECT COUNT(*) FROM {$table}";
        $out[$table] = $wpdb->get_var($sql);
    }
    return $out;
}

function check_previous_stage_complete()
{
    global $wpdb;
    $stage = strtolower($_GET['stage']);
    $previous_stage_table_array = [
            'close' => 'crm_negotiations',
            'negotiations' => 'crm_proposal',
            'proposal' => 'crm_analysis',
    ];
    $table = $previous_stage_table_array[$stage];
    $transaction_id = $_COOKIE['transaction_id'];
    $sql = <<<sql
SELECT id FROM {$table} WHERE transaction_id = {$transaction_id}
sql;
    $result = $wpdb->get_var($sql);
    if ($result) {
        echo 'ok'; die();
    }
    else {
        echo mb_ucfirst(str_replace('crm_', '', $previous_stage_table_array[$stage]));
        die();
    }
}

function register_custom_user_column($columns)
{

    $columns['relotis_user2'] = 'Relotis User';
    $columns['plan'] = 'Sales Target, $';
    $columns['save_plan'] = 'Save';
    var_dump($columns);
    unset($columns['posts']);
    return $columns;
}
function register_custom_user_column_view($value, $column_name, $user_id)
{
    $user_info = get_userdata( $user_id );
    if($column_name == 'plan') return  '<input style="width: 110px;" class="form-control plan" value="' . $user_info->plan . '">';
    if($column_name == 'save_plan') return '<button class="btn btn-info" data-user-id="' . $user_id . '">Save</button>';
    if($column_name == 'relotis_user2') return '<a href="/wp-admin/admin.php?page=relotis_user2&wp_user_id=' . $user_id . '">Link</a>';
    return $value;
}

function save_user_plan()
{
    global $wpdb;
    $id = $_POST['id'];
    $plan = $_POST['plan'];
    $sql = <<<sql
UPDATE wp_users SET plan = $plan WHERE id = $id
sql;
    $wpdb->query($sql);
    echo 'ok';
    die();
}

function relotis_user3()
{
    global $wpdb;
    global $plan;
    $plan = wp_get_current_user()->plan;
    ?>
    <script>
    var plan = <?=$plan?>
</script>
<?
    $wp_user_id = $_GET['wp_user_id']??wp_get_current_user()->ID;
    $user = get_user_by ('ID', $wp_user_id);
    $sql = <<<sql
SELECT SUM(price_value) FROM crm_analysis WHERE wp_user_id = {$wp_user_id}
sql;
    $sum = $wpdb->get_var($sql);
    $sql = <<<sql
SELECT plan FROM wp_users WHERE `ID`=$wp_user_id
sql;
    $user_plan = $wpdb->get_var($sql);

    ?>
<h3><?=wp_get_current_user()->display_name?></h3>
<table>
<tbody>
<tr>
<td>
<table class="table" style="width: auto;">
    <thead>
            <tr>
                <th>Login</th>
                <th>Email</th>
                <th>Name</th>
                <th>Total deal's SUM</th>
                <th>Plan</th>

            </tr>
    </thead>
    <tbody>
    <tr>
    <td><?=$user->user_login?></td>
    <td><?=$user->user_email?></td>
    <td><?=$user->display_name?></td>
    <td><?=$sum?></td>
    <td><?=$user_plan?></td>
</tr>
</tbody>
</table>

</td>
<td>

</td>
</tr>
</tbody>
</table>
<table class="table">
<tbody>
<tr>
<td style="width: 50%">
<?=do_shortcode('[sema4 title="My Stages" wp_user_id="' . wp_get_current_user()->ID .'"]')?>
</td>
<td id="meter" style="width: 50%; padding-bottom: 100px;">
<? $value = round($sum/$user_plan*100)?>
<?=do_shortcode('[common_chart_css_js]')?>
<?=do_shortcode('[virtual_meter width="200" height="200" value="' . $value . '"]')?>
</td>
</tr>
</tbody>
</table>
<hr>
<?
    $sql = <<<sql
SELECT * FROM crm_analysis WHERE wp_user_id = $wp_user_id
sql;
$data = $wpdb->get_results($sql);
?>
<h3>My Deals</h3>
<table class="table table-hover table-striped table-bordered">
<thead>
<tr>
    <th>CRM ID<br> &link</th>
    <th>Price</th>
    <th>Address</th>
    <th>Image</th>
    <th>Listing's<br>link</th>
    <th>Status</th>
</tr>
</thead>
<tbody>
<?foreach ($data as $row):?>
<tr>
<td>
<a href="<?=plugin_dir_url(__FILE__)?>linker.php?transaction_id=<?=$row->transaction_id?>&wp_user_id=<?=$row->wp_user_id?>&wp_user_name=<?=$row->wp_user_name?>&status=<?=$row->status?>">
    <?=$row->transaction_id?></a>
</td>
    <td><?=$row->price_value?></td>
    <td><?=$row->address?></td>
    <td><a href="<?=$row->image_src?>"> <img src="<?=$row->image_src?>" style="height: 100px;"></a></td>
    <td><a href="<?=$row->object_url?>" target="_blank" >Link</td>
    <td><?=$row->status?></td>
</tr>
<?endforeach; ?>
</tbody>
</table>
<table style="width: 100%">
<tbody>
<tr>
<td>

</td>
<td>
</td>
</tr>
</tbody>
</table>
    <?
}

function dashboard_view($html)
{
    if($_SERVER['REQUEST_URI'] == '/wp-admin/' || $_SERVER['REQUEST_URI'] == '/wp-admin')
    {
            ?>
            <script>
            $(document).ready(function(){
                setTimeout(function() {
                  $('#wpcontent').html('<?=$html?>');
                });
            })
            </script>
    <?
    }
}

function crm_plugin_init()
{
    crm_db_init();
    crm_file_init();
}